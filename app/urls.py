from django.conf.urls import url
from . import views
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token

from .views import AttractionViewSet, CategoryViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'attractions', AttractionViewSet, basename='attractions')
router.register(r'categories', CategoryViewSet, basename='categories')
urlpatterns = router.urls

urlpatterns.extend([
    url(r'^token-auth/', obtain_jwt_token),
    url(r'^token-verify/', verify_jwt_token),
    url(r'^register/', views.RegisterUsers.as_view())
])
