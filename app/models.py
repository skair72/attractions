from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class Attraction(models.Model):
    # TODO: Comments class
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    latitude = models.FloatField()
    longitude = models.FloatField()
    description = models.TextField(blank=True, default='')
    rating = models.FloatField(blank=True, default=0)
    image = models.ImageField(
        upload_to='images/',
        max_length=254, blank=True, null=True
    )

    categories = models.ManyToManyField('Category', blank=True, related_name='attractions')
    bookmarked = models.ManyToManyField('User', blank=True, related_name='bookmarks')

    class Meta:
        ordering = ('created',)


class Category(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)

    class Meta:
        ordering = ('created',)


class User(AbstractUser):
    pass
