from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from django.contrib.auth import authenticate

from app.models import (
    Category,
    Attraction,
    User
)

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


def get_jwt_token(user: User, password: str) -> dict:
    authenticating = authenticate(username=user.username, password=password)
    print(authenticating)
    if authenticating:
        if not user.is_active:
            raise serializers.ValidationError({'error': 'User account is disabled.'}, code='authorization')

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return {'token': token}

    else:
        raise serializers.ValidationError({'error': 'Unable to log in with provided credentials.'},
                                          code='authorization')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    bookmarks = serializers.PrimaryKeyRelatedField(many=True,
                                                   queryset=Attraction.objects.all(),
                                                   required=False,
                                                   default=[])

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'bookmarks')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        if 'bookmarks' in validated_data:
            del validated_data['bookmarks']
        user = User.objects.create(**validated_data)
        user.set_password(validated_data.get('password'))
        user.save()
        return user


class CategorySerializer(serializers.ModelSerializer):
    attractions = serializers.PrimaryKeyRelatedField(many=True,
                                                     queryset=Attraction.objects.all(),
                                                     required=False,
                                                     default=[])

    class Meta:
        model = Category
        fields = "__all__"


class AttractionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attraction
        fields = "__all__"
