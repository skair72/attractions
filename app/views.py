from .models import Attraction, Category
from .serializers import AttractionSerializer, CategorySerializer, UserSerializer, get_jwt_token
from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ParseError


def generate_permissions(cls, allowedForAuthenticated):
    if cls.action in allowedForAuthenticated:
        _permission_classes = [IsAuthenticated]
    else:
        _permission_classes = [IsAdminUser]
    return [permission() for permission in _permission_classes]


class AttractionViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given attraction.

    list:
    Return a list of all the existing attractions.
    Query parameter `categories`, Example: `categories=1,2`

    create:
    Create a new user instance.
    """

    queryset = Attraction.objects.all()
    serializer_class = AttractionSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        return generate_permissions(self, ['list', 'retrieve', 'bookmark', 'bookmarks'])

    @action(detail=True, methods=['put', 'delete', 'get'])
    def bookmark(self, request, *args, **kwargs):
        """
        get:
        Returns ids of users which bookmarked that attraction

        put:
        Puts the attraction to bookmarks of the user

        delete:
        Removes the attraction from bookmarks of the user
        """
        attraction = self.get_object()

        if request.method == 'GET':
            return Response(AttractionSerializer(self.get_object()).data.pop('bookmarked'),
                            status=status.HTTP_200_OK)

        if request.method == 'PUT':
            attraction.bookmarked.add(request.user)
            return Response(status=status.HTTP_204_NO_CONTENT)

        if request.method == 'DELETE':
            attraction.bookmarked.remove(request.user)
            return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['get'])
    def bookmarks(self, request, *args, **kwargs):
        """
        Returns all bookmarked attractions of the user
        """
        queryset = Attraction.objects.all()
        bookmarked = queryset.filter(bookmarked__in=[request.user.id])
        return Response(AttractionSerializer(bookmarked, many=True).data,
                        status=status.HTTP_200_OK)

    @action(detail=True, methods=['put', 'delete', 'get'])
    def image(self, request, *args, **kwargs):
        """
        get:
        Returns image location if exists
        put:
        Set attraction image
        delete:
        Removes the attraction image
        """

        attraction = self.get_object()

        if request.method == 'GET':
            image = attraction.image
            return Response(AttractionSerializer(attraction).data['image'] if image else None,
                            status=status.HTTP_200_OK if image else status.HTTP_404_NOT_FOUND)

        attraction.image.delete()

        if request.method == 'PUT':
            try:
                file = request.data['file']
            except KeyError:
                raise ParseError('Request has no resource file attached')
            attraction.image = file
            attraction.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given categories,
        by filtering against a `categories` query parameter in the URL.
        """
        queryset = Attraction.objects.all()
        categories = self.request.query_params.get('categories')
        if categories is not None:
            queryset = queryset.filter(categories__in=categories.split(',')).distinct()
        return queryset


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        return generate_permissions(self, ['list', 'retrieve'])

    def apply_to_attraction(self, ):
        pass

    @action(detail=True, methods=['put', 'delete'])
    def attraction(self, request, *args, **kwargs):
        """
        put:
        Add the category to attractions.
        Query params `?ids=1,2,3`

        delete:
        Removes the category from attractions
        Query params `?ids=1,2,3`
        """
        category = self.get_object()

        attractions_ids = request.query_params.get('ids')
        if attractions_ids is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        attractions = Attraction.objects.all().filter(pk__in=attractions_ids.split(','))

        if request.method == 'PUT':
            category.attractions.add(*attractions)

        if request.method == 'DELETE':
            category.attractions.remove(*attractions)

        return Response(status=status.HTTP_204_NO_CONTENT)


class RegisterUsers(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        """
        Registering a new user and returns JWT token
        """
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            return Response(get_jwt_token(serializer.save(),
                                          request.data.get('password')),
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
