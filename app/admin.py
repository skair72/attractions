from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Attraction, Category

admin.site.register(User, UserAdmin)
admin.site.register(Attraction)
admin.site.register(Category)
