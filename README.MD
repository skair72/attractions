## Регистрация и аутентифкация
* Регистрация  
POST http://127.0.0.1:8000/api/register/  
```JSON
{
  "username": "user",  
  "password": "password"  
}
```
Ответ:  
`
{
    "token": "***token***"
}
`

* Аутентификация    
POST http://127.0.0.1:8000/api/token-auth/  
```JSON
{
	"username": "admin",
	"password": "admin"
}
```
Овет:  
`
{
    "token": "***token***"
}
`  
В дальнейшем для работы с апи нужно добавить cледующий заголовок:  
Authorization: Bearer ***token***  

## Работа с достопримечательностями
* Добавить достопримечательность  
POST http://127.0.0.1:8000/api/attractions/  
```JSON
{
    "name": "Сквер сибирских кошек",
    "latitude": 57.154015,
    "longitude": 65.536637,
    "description": "description",
    "rating": 0
}
```

* Список достопримечательностей  
GET http://127.0.0.1:8000/api/attractions/

* Фильтрация по категориям  
GET http://127.0.0.1:8000/api/attractions?categories=1,2

* Добавить в закладки  
PUT http://127.0.0.1:8000/api/attractions/1/bookmark/

* Удалить из закладок  
DELETE http://127.0.0.1:8000/api/attractions/1/bookmark/

* Удалить достопримечательность  
DELETE http://127.0.0.1:8000/api/attractions/1/

* Установить изображение  
PUT http://127.0.0.1:8000/api/attractions/2/image/  
Body:  
    file: файл для загрузки (метод прошел тест через Postman)  

* Удалить изображение  
DELETE http://127.0.0.1:8000/api/attractions/2/image/

## Работа с категориями
* Добавить категорию  
POST http://127.0.0.1:8000/api/attractions/
```JSON
{
	"name": "test category"
}
```  

* Добавить достопримечательности в категорию  
PUT http://127.0.0.1:8000/api/categories/3/attraction/?ids=1,2,3

* Удалить достопримечательности из категории  
DELETE http://127.0.0.1:8000/api/categories/3/attraction/?ids=1,2,3

Сгенерированная документация: 
http://127.0.0.1:8000/docs/

